# CERN HTML Template

This repository contains the most basic template for a CERN website. Please use this as a reference for static websites.

The header and footer, as well as logo and favicon, are in accordance with CERN's Design Guidelines.

![Screenshot of CERN HTML Template](screenshot.png)
